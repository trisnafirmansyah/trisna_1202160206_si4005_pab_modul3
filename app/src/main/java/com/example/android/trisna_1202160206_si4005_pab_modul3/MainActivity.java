package com.example.android.trisna_1202160206_si4005_pab_modul3;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.os.Parcelable;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.support.v7.widget.helper.ItemTouchHelper;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.EditText;
import android.widget.Spinner;

import java.util.ArrayList;
import java.util.Collections;

public class MainActivity extends AppCompatActivity {

    private RecyclerView recyclerView;
    private RecyclerView.LayoutManager layoutManager;


    AlertDialog.Builder dialog;
    LayoutInflater inflater;
    View dialogView;
    String nama, pekerjaan, kelamin;
    Parcelable savedRecyclerLayoutState;
    String LIST_STATE = "list state";
    ArrayList<Akun> listAkun;
    EditText txt_nama, txt_pekerjaan;
    Spinner txt_kelamin;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        listAkun = new ArrayList<>();

        RecyclerView myrv = (RecyclerView) findViewById(R.id.recyclerview_id);
        final RecyclerViewAdapter myAdapter = new RecyclerViewAdapter(this,listAkun);
        myrv.setAdapter(myAdapter);
        int gridColumnCount = getResources().getInteger(R.integer.grid_column_count);
        myrv.setLayoutManager(new GridLayoutManager(this, gridColumnCount));
        ItemTouchHelper helper = new ItemTouchHelper(new ItemTouchHelper.SimpleCallback
                (ItemTouchHelper.LEFT | ItemTouchHelper.RIGHT | ItemTouchHelper.DOWN
                        | ItemTouchHelper.UP, ItemTouchHelper.LEFT | ItemTouchHelper.RIGHT) {
            @Override
            public boolean onMove(RecyclerView recyclerView, RecyclerView.ViewHolder viewHolder,
                                  RecyclerView.ViewHolder target) {

                //Get the from and to position
                int from = viewHolder.getAdapterPosition();
                int to = target.getAdapterPosition();

                //Swap the items and notify the adapter
                Collections.swap(listAkun, from, to);
                myAdapter.notifyItemMoved(from, to);
                return true;
            }

            @Override
            public void onSwiped(RecyclerView.ViewHolder viewHolder, int direction) {

                //Remove the item from the dataset
                listAkun.remove(viewHolder.getAdapterPosition());

                //Notify the adapter
                myAdapter.notifyItemRemoved(viewHolder.getAdapterPosition());
            }
        });
        helper.attachToRecyclerView(myrv);
        FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.floatingActionButton2);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                DialogForm();
            }
        });

    }



    private void DialogForm() {
        dialog = new AlertDialog.Builder(MainActivity.this);
        inflater = getLayoutInflater();
        dialogView = inflater.inflate(R.layout.addakun_, null);
        dialog.setView(dialogView);
        dialog.setCancelable(true);
        dialog.setTitle("Form Akun Baru");

        txt_nama    = (EditText) dialogView.findViewById(R.id.txt_nama);
        txt_pekerjaan    = (EditText) dialogView.findViewById(R.id.txt_pekerjaan);
        txt_kelamin  = (Spinner) dialogView.findViewById(R.id.txt_kelamin);


        dialog.setPositiveButton("SUBMIT", new DialogInterface.OnClickListener() {

            @Override
            public void onClick(DialogInterface dialog, int which) {
                nama    = txt_nama.getText().toString();
                pekerjaan    = txt_pekerjaan.getText().toString();
                kelamin = txt_kelamin.getSelectedItem().toString();

                if (kelamin.equals("Laki-Laki"))
                    listAkun.add(new Akun(nama, pekerjaan,
                            kelamin,R.drawable.man));
                else
                    listAkun.add(new Akun(nama, pekerjaan,
                            kelamin,R.drawable.woman));

                dialog.dismiss();
            }
        });

        dialog.setNegativeButton("CANCEL", new DialogInterface.OnClickListener() {

            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
            }
        });

        dialog.show();
    }
}
